/*
  @(#)Application.java 1.00 12/11/2018

  Copyright (c) Rostyslav Pikulyk

  Application for EPAM course
  This application generate Fibonacci sequence
 */

package com.pikulyk;

/**
 * <h1>Fibonacci</h1>
 * The Fibonacci program implements an application that simply displays:
 * - odd numbers from start to the end of interval and even from end to start;
 * - the sum of odd and even numbers;
 * - Fibonacci numbers;
 * - the biggest odd number and the biggest even number;
 * - percentage of odd and even Fibonacci numbers.
 * <p>
 * There are four classes in this program. The main class is Application,
 * in which our program starts.
 *
 * @author Rostyslav Pikulyk <ross.pikulyk@gmail.com>
 * @version 1.0
 * @since 2018-11-12
 */
public final class Application {

  /**
   * Constructor without parameters.
   */
  private Application() {
  }

  /**
   * The main method which make use of runMenu method.
   *
   * @param args Unused.
   */
  public static void main(final String[] args) {
    Menu menu = new Menu();
    menu.runMenu();
  }
}
