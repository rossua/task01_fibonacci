package com.pikulyk;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Class Fibonacci builds Fibonacci set entered by user.
 * Also prints the biggest odd, even number and
 * prints percentage of odd and even Fibonacci numbers.
 */

public class Fibonacci {

  /**
   * ONE_HUNDRED_PERCENTS is constant.
   */
  private static final int ONE_HUNDRED_PERCENTS = 100;
  /**
   * hashMapCache helps to improve our recursive method.
   */
  private static HashMap<Integer, Long> hashMapCache = new HashMap<>();

  /**
   * This method is main method which starts our Fibonacci program.
   * Makes use of next methods:
   * getPercentageOfEven, getPercentageOfOdd,
   * getTheBiggestEven, getTheBiggestOdd.
   *
   * @param numberOfFibonacciSet int is self-explained.
   */
  final void runFibonacci(final int numberOfFibonacciSet) {
    long[] arrayForFibonacciNumbers = new long[numberOfFibonacciSet];

    for (int i = 1; i <= numberOfFibonacciSet; i++) {
      arrayForFibonacciNumbers[i - 1] = calculateFiboNums(i);
    }
    System.out.println(Arrays.toString(arrayForFibonacciNumbers));
    System.out.println("\n" + getPercentageOfEven(arrayForFibonacciNumbers)
        + "\n" + getPercentageOfOdd(arrayForFibonacciNumbers)
        + "\n" + getTheBiggestEven(arrayForFibonacciNumbers)
        + "\n" + getTheBiggestOdd(arrayForFibonacciNumbers));
  }

  /**
   * This recursive method returns fibonacci number by giving amount of set.
   *
   * @param amountOfSet int defines how many numbers we want to get.
   * @return long latest number is our entered set.
   */
  private long calculateFiboNums(final int amountOfSet) {
    long result;

    if (hashMapCache.containsKey(amountOfSet)) {
      return hashMapCache.get(amountOfSet);
    }
    if (amountOfSet == 0) {
      return 0;
    } else if (amountOfSet == 1) {
      return 1;
    }

    result = calculateFiboNums(amountOfSet - 2)
        + calculateFiboNums(amountOfSet - 1);
    hashMapCache.put(amountOfSet, (long) result);
    return result;
  }

  /**
   * This method gets the biggest odd number from fibonacci sequence
   * by iterating throught given array.
   *
   * @param fibonacciNumbers long array.
   * @return long that is biggest odd number.
   */
  private long getTheBiggestOdd(final long[] fibonacciNumbers) {
    long theBiggestOdd = 0;

    for (long number : fibonacciNumbers) {
      if (number > theBiggestOdd && number % 2 == 1) {
        theBiggestOdd = number;
      }
    }

    return theBiggestOdd;
  }

  /**
   * This method gets the biggest even number from fibonacci sequence
   * by iterating throught given array.
   *
   * @param fibonacciNumbers long array.
   * @return long - biggest even number.
   */
  private long getTheBiggestEven(final long[] fibonacciNumbers) {
    long theBiggestEven = 0;

    for (long number : fibonacciNumbers) {
      if (number > theBiggestEven && number % 2 == 0) {
        theBiggestEven = number;
      }
    }
    return theBiggestEven;
  }

  /**
   * This method get percentage of even fibonacci numbers.
   *
   * @param fibonacciNumbers long array.
   * @return float - percentage of even numbers in array.
   */
  private float getPercentageOfEven(final long[] fibonacciNumbers) {
    float percentage;
    int countOfEvenNumbers = 0;

    for (long number : fibonacciNumbers) {
      if (number % 2 == 0) {
        countOfEvenNumbers++;
      }
    }
    percentage = (float) (countOfEvenNumbers * ONE_HUNDRED_PERCENTS)
        / fibonacciNumbers.length;
    return percentage;
  }

  /**
   * This method get percentage of odd fibonacci numbers.
   *
   * @param fibonacciNumbers long array.
   * @return float - percentage of odd numbers in array.
   */
  private float getPercentageOfOdd(final long[] fibonacciNumbers) {
    return ONE_HUNDRED_PERCENTS - getPercentageOfEven(fibonacciNumbers);
  }
}



