package com.pikulyk;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class Menu prints menu in terminal in which user
 * can choose what kind of program to start.
 */

public class Menu {

  /**
   * exit variable comes out of the program when value = true.
   * Takes true if user's choice = 0.
   */
  private boolean exit = false;
  /**
   * calculation variable helps to start our Calculation program.
   */
  private Calculation calculation = new Calculation();
  /**
   * fibonacci variable helps to start our Fibonacci program.
   */
  private Fibonacci fibonacci = new Fibonacci();
  /**
   * scanner variable used to takes user intput from keyboard.
   */
  private Scanner scanner = new Scanner(System.in);

  /**
   * This is the main method which makes use of next methods:
   * printHeader , printMenu, getMenuChoice , performAction.
   */
  final void runMenu() {
    printHeader();
    while (!exit) {
      printMenu();
      int choice = getMenuChoice();
      performAction(choice);
    }
  }

  /**
   * This method prints our menu head.
   */
  private void printHeader() {
    System.out.println("+--------------------------------+\n"
        + "|           Welcome to           |\n"
        + "|              Menu              |\n"
        + "+--------------------------------+");
  }

  /**
   * This method prints instructions for user.
   */
  private void printMenu() {
    System.out.println("\nPlease make a selection: \n"
        + "1) Print odd and even numbers\n"
        + "2) Build Fibonacci numbers\n"
        + "0) Exit\n");
  }

  /**
   * This method returns user's choice if it's legal,
   * if not - complaints and user should enter his/her choice again.
   *
   * @return int This returns entered from keyboard user's choice.
   */
  private int getMenuChoice() {
    int choice = -1;

    do {
      try {
        choice = scanner.nextInt();
      } catch (InputMismatchException e) {
        System.out.println("Invalid selection. Numbers only please.");
        scanner.next();
        continue;
      }
      if (choice < 0 || choice > 2) {
        System.out.println("Choice outside of range. Please choose again.");
      }
    } while (choice < 0 || choice > 2);
    return choice;
  }

  /**
   * This method launches other programs depending on the user's choice
   * that is passed as a parameter.
   *
   * @param choice pass user's choice entered in previous mehtod.
   */
  private void performAction(final int choice) {
    int numberOfFibonacciSet;
    int startOfArray;
    int endOfArray;

    switch (choice) {
      case 0:
        exit = true;
        System.out.println("Thank you for using my application.");
        break;
      case 1:
        System.out.println("Enter start of the array: ");
        startOfArray = scanner.nextInt();
        System.out.println("Enter end of the array: ");
        endOfArray = scanner.nextInt();
        calculation.runCalculation(startOfArray, endOfArray);
        break;
      case 2:
        System.out.println("Enter the size of set: ");
        numberOfFibonacciSet = scanner.nextInt();
        fibonacci.runFibonacci(numberOfFibonacciSet);
        break;
      default:
        System.out.println("errror");
        break;
    }
  }
}
