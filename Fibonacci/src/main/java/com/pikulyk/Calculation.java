package com.pikulyk;

/**
 * Class Calculation takes some interval and calculate:
 * - odd numbers from start to the end and even from end to start;
 * - the sum of odd and even numbers.
 */

public class Calculation {

  /**
   * arrOfNumbers int array used to put all numbers from entered interval.
   */
  private int[] arrOfNumbers;
  /**
   * lengthOfArray int self-explained.
   */
  private int lengthOfArray;

  /**
   * This method is main method which starts our Calculation program.
   * Makes use of next methods:
   * printOddAscending, printEvenDescending, printSumOfOddAndEven.
   *
   * @param start int start of our interval.
   * @param end int end of our interval.
   */
  final void runCalculation(final int start, final int end) {
    lengthOfArray = end - start + 1;
    arrOfNumbers = new int[lengthOfArray];
    for (int i = 0; i < lengthOfArray; i++) {
      arrOfNumbers[i] = start + i;
    }
    printOddAscending(arrOfNumbers);
    printEvenDescending(arrOfNumbers);
    printSumOfOddAndEven(arrOfNumbers);
  }

  /**
   * This method checks if our number is even or odd.
   *
   * @param number int number to check.
   * @return boolean outputs the result depending on calculation.
   */
  private boolean isEvenNumber(final int number) {
    return number % 2 == 0;
  }

  /**
   * This method prints all odd numbers by ascending order.
   *
   * @param arr int array in which we find our numbers.
   */
  private void printOddAscending(final int[] arr) {
    System.out.println("Odd Ascending: ");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');

    for (int i = 0; i < arr.length; i++) {
      if (!isEvenNumber(arr[i])) {
        stringBuilder.append(arr[i]);
        if (i >= (arr.length - 2)) {
          stringBuilder.append(']');
          break;
        }
        stringBuilder.append(", ");
      }
    }
    System.out.println(stringBuilder.toString());
  }

  /**
   * This method prints all even numbers by descending order.
   *
   * @param arr int array in which we find our numbers.
   */
  private void printEvenDescending(final int[] arr) {
    System.out.println("Even Descending: ");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append('[');

    for (int i = arr.length - 1; i >= 0; i--) {
      if (isEvenNumber(arr[i])) {
        stringBuilder.append(arr[i]);
        if (i <= 1) {
          stringBuilder.append(']');
          break;
        }
        stringBuilder.append(", ");
      }
    }
    System.out.println(stringBuilder.toString());
  }

  /**
   * This method prints sum of all odd and sum of all even numbers.
   *
   * @param arr int array in which we find our sum.
   */
  private void printSumOfOddAndEven(final int[] arr) {
    int sumOfOdd = 0;
    int sumOfEven = 0;

    for (int i = 0; i < arr.length; i++) {
      if (!isEvenNumber(arr[i])) {
        sumOfOdd += arr[i];
      } else {
        sumOfEven += arr[i];
      }
    }
    System.out.println("Sum of Odd = " + sumOfOdd);
    System.out.println("Sum of Even = " + sumOfEven);
  }
}

