/*
  @(#)Application.java 1.00 12/11/2018

  Copyright (c) Rostyslav Pikulyk

  Application for EPAM course
  This application generate Fibonacci sequence
 */

/**
 * Package for this app.
 */
package com.pikulyk;
